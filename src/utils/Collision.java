package utils;

import entities.Dinosaur;
import entities.Cactus;
import entities.Bird;

public class Collision {
    public static boolean checkCollision(Dinosaur dinosaur, Cactus cactus) {
        return dinosaur.getX() < cactus.getX() + cactus.getWidth() &&
                dinosaur.getX() + dinosaur.getWidth() > cactus.getX() &&
                dinosaur.getY() < cactus.getY() + cactus.getHeight() &&
                dinosaur.getHeight() + dinosaur.getY() > cactus.getY();
    }

    public static boolean checkCollision(Dinosaur dinosaur, Bird bird) {
        return dinosaur.getX() < bird.getX() + bird.getWidth() &&
                dinosaur.getX() + dinosaur.getWidth() > bird.getX() &&
                dinosaur.getY() < bird.getY() + bird.getHeight() &&
                dinosaur.getHeight() + dinosaur.getY() > bird.getY();
    }
}
