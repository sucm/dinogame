package entities;

import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Graphics;

public class Dinosaur {
    private int x, y;
    private int width, height;
    private int runningHeight, duckingHeight;
    private Image runningImage1;
    private Image runningImage2;
    private Image duckingImage;
    private boolean isDucking = false;
    private int velocityY = 0;
    private final int gravity = 5;
    private boolean onGround = true;
    private int duckingY; // Přidáno pro ukládání y-pozice při pokrčení
    private int animationCounter = 0; // Počítadlo pro animaci
    private final int ANIMATION_SPEED = 5; // Rychlost animace, zkráceno pro rychlejší animaci

    public Dinosaur(int x, int y) {
        this.x = x;
        this.y = y;
        ImageIcon runningIcon1 = new ImageIcon("resources/images/dinosaur_running1.png");
        this.runningImage1 = runningIcon1.getImage();
        ImageIcon runningIcon2 = new ImageIcon("resources/images/dinosaur_running2.png");
        this.runningImage2 = runningIcon2.getImage();
        ImageIcon duckingIcon = new ImageIcon("resources/images/dinosaur_ducking.png");
        this.duckingImage = duckingIcon.getImage();
        this.width = runningImage1.getWidth(null);
        this.runningHeight = runningImage1.getHeight(null);
        this.duckingHeight = duckingImage.getHeight(null);
        this.height = runningHeight;
        this.duckingY = y + (runningHeight - duckingHeight); // Nastavení počáteční y-pozice při pokrčení
    }

    public void update() {
        if (!onGround && !isDucking) {
            y += velocityY;
            velocityY += gravity;
        }

        if (y > 150) {
            y = 150;
            velocityY = 0;
            onGround = true;
        }

        if (isDucking) {
            y = duckingY;
        }

        animationCounter++;
    }

    public void jump() {
        if (onGround && !isDucking) {
            velocityY = -40;
            onGround = false;
        }
    }

    public void duck(boolean ducking) {
        isDucking = ducking;
        if (isDucking) {
            height = duckingHeight;
            y = duckingY;
        } else {
            height = runningHeight;
            y = 150;
        }
    }

    public void draw(Graphics g) {
        if (isDucking) {
            g.drawImage(duckingImage, x, duckingY, null);
        } else {
            Image currentRunningImage = (animationCounter / ANIMATION_SPEED % 2 == 0) ? runningImage1 : runningImage2;
            g.drawImage(currentRunningImage, x, y, null);
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return isDucking ? duckingY : y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setY(int y) {
        this.y = y;
    }
}
