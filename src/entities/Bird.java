package entities;

import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Graphics;

public class Bird {
    private int x, y;
    private int width, height;
    private Image flyingImage1;
    private Image flyingImage2;
    private int animationCounter = 0; // Počítadlo pro animaci
    private final int ANIMATION_SPEED = 5; // Rychlost animace, zkráceno pro rychlejší animaci

    public Bird(int x, int y) {
        this.x = x;
        this.y = y;
        ImageIcon flyingIcon1 = new ImageIcon("resources/images/bird_flying1.png");
        this.flyingImage1 = flyingIcon1.getImage();
        ImageIcon flyingIcon2 = new ImageIcon("resources/images/bird_flying2.png");
        this.flyingImage2 = flyingIcon2.getImage();
        this.width = flyingImage1.getWidth(null);
        this.height = flyingImage1.getHeight(null);
    }

    public void update(int speed) {
        x -= speed;
        animationCounter++;
    }

    public void draw(Graphics g) {
        Image currentFlyingImage = (animationCounter / ANIMATION_SPEED % 2 == 0) ? flyingImage1 : flyingImage2;
        g.drawImage(currentFlyingImage, x, y, null);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
