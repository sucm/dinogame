package entities;

import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Graphics;

public class Cactus {
    private int x, y;
    private int width, height;
    private Image image;

    public Cactus(int x, int y) {
        this.x = x;
        this.y = y;
        ImageIcon ii = new ImageIcon("resources/images/cactus.png");
        this.image = ii.getImage();
        this.width = image.getWidth(null);
        this.height = image.getHeight(null);
    }

    public void update(int speed) {
        x -= speed; // Snížení x o rychlost hry
    }

    public void draw(Graphics g) {
        g.drawImage(image, x, y, null);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
