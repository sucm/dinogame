package game;

import javax.swing.JFrame;

public class GameWindow extends JFrame {
    public GameWindow() {
        this.add(new GamePanel());
        this.setTitle("Dinosaur Game");
        this.setResizable(false);
        this.pack();  // Tato metoda nastaví velikost okna podle preferovaných rozměrů komponent uvnitř.
        this.setLocationRelativeTo(null);  // Centrování okna na obrazovce.
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new GameWindow();  // Vytvoření instance okna, která spustí hru.
    }
}
