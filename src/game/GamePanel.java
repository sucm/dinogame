package game;

import entities.Dinosaur;
import entities.Cactus;
import entities.Bird;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;

public class GamePanel extends JPanel implements ActionListener {
    private Dinosaur dinosaur;
    private List<Cactus> cacti;
    private List<Bird> birds;
    private Timer timer;
    private int gameSpeed = 15; // Nastavení počáteční rychlosti hry
    private long lastCactusTime = 0;
    private long lastBirdTime = 0;
    private int cactusInterval = 2000; // Počáteční interval v ms pro generování nových kaktusů
    private int birdInterval = 5000; // Počáteční interval v ms pro generování nových ptáků
    private int score = 0;
    private int highScore = 0; // Proměnná pro nejvyšší skóre
    private Random random;
    private boolean gameOver = false;
    private static final int MIN_BIRD_DISTANCE = 300; // Minimální vzdálenost ptáků od kaktusů
    private static final int BIRD_Y_POSITION = 83; // Vyšší pevná výška pro všechny ptáky
    private static final int MIN_CACTUS_DISTANCE = 600; // Konstantní minimální vzdálenost mezi kaktusy
    private static final int INITIAL_MAX_CACTUS_DISTANCE = 1200; // Maximální vzdálenost na začátku hry
    private static final int DOUBLE_CACTUS_EXTRA_DISTANCE = 300; // Extra vzdálenost po dvojitých kaktusech

    public GamePanel() {
        setPreferredSize(new Dimension(1200, 300));
        dinosaur = new Dinosaur(100, 150);
        cacti = new ArrayList<>();
        birds = new ArrayList<>();
        timer = new Timer(20, this);
        timer.start();
        lastCactusTime = System.currentTimeMillis();
        lastBirdTime = System.currentTimeMillis();
        random = new Random();

        setFocusable(true);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    if (gameOver) {
                        resetGame();
                    } else {
                        dinosaur.jump();
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    dinosaur.duck(true);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    dinosaur.duck(false);
                }
            }
        });
    }

    public void actionPerformed(ActionEvent e) {
        if (!gameOver) {
            updateGame();
            repaint();
        }
    }

    private void updateGame() {
        dinosaur.update();

        for (Cactus cactus : cacti) {
            cactus.update(gameSpeed);
        }

        for (Bird bird : birds) {
            bird.update(gameSpeed);
        }

        cacti.removeIf(c -> c.getX() + c.getWidth() < 0);
        birds.removeIf(b -> b.getX() + b.getWidth() < 0);

        long currentTime = System.currentTimeMillis();

        if (currentTime - lastCactusTime > cactusInterval) {
            int maxDistance = Math.max(MIN_CACTUS_DISTANCE, INITIAL_MAX_CACTUS_DISTANCE - score / 10);
            int distance = random.nextInt(maxDistance - MIN_CACTUS_DISTANCE + 1) + MIN_CACTUS_DISTANCE;

            if (random.nextInt(10) < 2) { // 20% šance na dvojité kaktusy
                int cactusWidth = new Cactus(0, 0).getWidth();
                Cactus firstCactus = new Cactus(1200, 170);
                Cactus secondCactus = new Cactus(1200 + cactusWidth, 170);
                cacti.add(firstCactus);
                cacti.add(secondCactus);
                // Zajištění, že další kaktus bude generován s minimální vzdáleností od druhého kaktusu
                int lastCactusEnd = secondCactus.getX() + secondCactus.getWidth();
                lastCactusTime = currentTime;
                // Přidání dalšího kaktusu minimálně ve vzdálenosti MIN_CACTUS_DISTANCE + DOUBLE_CACTUS_EXTRA_DISTANCE od posledního kaktusu
                int nextCactusX = lastCactusEnd + MIN_CACTUS_DISTANCE + DOUBLE_CACTUS_EXTRA_DISTANCE;
                cacti.add(new Cactus(nextCactusX, 170));
                lastCactusTime += (nextCactusX - 1200) / gameSpeed * 20;
            } else {
                if (!cacti.isEmpty()) {
                    Cactus lastCactus = cacti.get(cacti.size() - 1);
                    int lastCactusEnd = lastCactus.getX() + lastCactus.getWidth();
                    int nextCactusX = Math.max(1200 + distance, lastCactusEnd + MIN_CACTUS_DISTANCE);
                    cacti.add(new Cactus(nextCactusX, 170));
                } else {
                    cacti.add(new Cactus(1200 + distance, 170)); // První kaktus v dané vzdálenosti
                }
                lastCactusTime = currentTime;
            }
        }

        if (score > 300 && currentTime - lastBirdTime > 3000) {  // Snížení skóre na 300 a intervalu na 3000 ms
            int birdX = 1200;
            boolean canPlaceBird = true;
            for (Cactus cactus : cacti) {
                if (Math.abs(cactus.getX() - birdX) < MIN_BIRD_DISTANCE) {
                    canPlaceBird = false;
                    break;
                }
            }
            if (canPlaceBird) {
                birds.add(new Bird(birdX, BIRD_Y_POSITION));
                lastBirdTime = currentTime;
                if (birdInterval > 2000) {
                    birdInterval -= 100; // Zrychlení generování ptáků s pokračujícím skóre
                }
            }
        }

        gameSpeed += 0.004; // Zrychlení hry
        score++;

        if (score % 100 == 0 && cactusInterval > 500) {
            cactusInterval -= 100;
        }

        if (score % 200 == 0 && birdInterval > 1500) {
            birdInterval -= 100;
        }

        if (checkCollision()) {
            gameOver = true;
            timer.stop();
            if (score > highScore) {
                highScore = score;
            }
        }
    }

    private boolean checkCollision() {
        for (Cactus cactus : cacti) {
            if (dinosaur.getX() < cactus.getX() + cactus.getWidth() &&
                    dinosaur.getX() + dinosaur.getWidth() > cactus.getX() &&
                    dinosaur.getY() < cactus.getY() + cactus.getHeight() &&
                    dinosaur.getY() + cactus.getHeight() > cactus.getY()) {
                return true;
            }
        }
        for (Bird bird : birds) {
            if (dinosaur.getX() < bird.getX() + bird.getWidth() &&
                    dinosaur.getX() + dinosaur.getWidth() > bird.getX() &&
                    dinosaur.getY() < bird.getY() + bird.getHeight() &&
                    dinosaur.getY() + bird.getHeight() > bird.getY()) {
                return true;
            }
        }
        return false;
    }

    private void resetGame() {
        gameOver = false;
        score = 0;
        gameSpeed = 15; // Nastavení počáteční rychlosti hry
        cactusInterval = 2000;
        birdInterval = 5000;
        cacti.clear();
        birds.clear();
        dinosaur = new Dinosaur(100, 150);
        lastCactusTime = System.currentTimeMillis();
        lastBirdTime = System.currentTimeMillis();
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        dinosaur.draw(g);
        for (Cactus cactus : cacti) {
            cactus.draw(g);
        }
        for (Bird bird : birds) {
            bird.draw(g);
        }

        /* Vykreslení hitboxů
        g.setColor(Color.RED);
        g.drawRect(dinosaur.getX(), dinosaur.getY(), dinosaur.getWidth(), dinosaur.getHeight());
        for (Cactus cactus : cacti) {
            g.drawRect(cactus.getX(), cactus.getY(), cactus.getWidth(), cactus.getHeight());
        }
        for (Bird bird : birds) {
            g.drawRect(bird.getX(), bird.getY(), bird.getWidth(), bird.getHeight());
        }*/

        g.drawString("Score: " + score, 10, 10);  // Zobrazení skóre
        if (gameOver) {
            g.setFont(g.getFont().deriveFont(24.0f)); // Zvětšení písma
            g.drawString("Game Over! Press UP to restart", 400, 150);  // Přidání textu Game Over uprostřed hry
            g.drawString("Your Score: " + score, 450, 180);  // Zobrazení skóre
            g.drawString("High Score: " + highScore, 450, 210);  // Zobrazení nejvyššího skóre
        }
    }
}
